#include "TutorialConfig.h"
#include "MathFunctions.h"
#include <cmath>
#include <cstdio>

double mysqrt(double val)
{
#if defined (HAVE_LOG) && defined (HAVE_EXP)
    fprintf(stderr, "Using exp and log functions to calculate sqrt\n");
    return exp(log(val)*0.5);
#else
    return sqrt(val);
#endif
}
